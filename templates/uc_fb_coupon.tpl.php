<?php

/**
 * @file
 * Facebook Likebox Template.
 */
?>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=756983860984262";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="fb-like-box" data-href="<?php echo $fb_url; ?>" data-width="<?php echo $fb_width; ?>" data-colorscheme="<?php echo $fb_colorscheme; ?>" data-show-faces="<?php echo $fb_show_faces; ?>" data-border-color data-stream="<?php echo $fb_stream; ?>" data-header="<?php echo $fb_header; ?>" data-height="<?php echo $fb_height; ?>" data-show-border="<?php echo $fb_show_border; ?>" data-force_wall="<?php echo $fb_force_wall; ?>" data-scrolling="<?php echo $fb_scrolling; ?>"></div>

<script>
(function ($) {
 $.getScript('//connect.facebook.net/en_UK/all.js', function(){
    FB.init({
      appId: '756983860984262',
      status     : true,                                
      xfbml      : true  
    }); 
 
  FB.Event.subscribe('edge.create',
    function(href, widget) {        
        $.cookie('islike', 1);
        location.reload();        
    });
});
})(jQuery)
</script>
